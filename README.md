## Hey <img src="https://github.com/TheDudeThatCode/TheDudeThatCode/blob/master/Assets/Hi.gif" width="29px">, We Are T-Dynamos


- 🔭 I’m currently working on [IgFreak](github.com/T-Dynamos/IgFreak)
- 🌱 I’m currently learning kivy framework
- 👯 I’m looking to collaborate on YouTube
- 💬 Ask me about Just Anything
- 📫 How to reach me: [YouTube](https://youtube.com/channel/UCCGprYqpszbeAYMGbjlh-aA)
- ⚡ Fun fact: Coding is not crime

### Join Telegram
* Group
[![image](https://github.com/T-Dynamos/T-Dynamos/raw/main/bin/20220113_103831.png)](https://t.me/TDynamos)
* PM
[On Telegram here](https://t.me/TDynamos)

### About Me 🚀
🌱 I’m a Full stack developer & I enjoy learning new things. </br>

![Aakash's github stats](https://github-readme-stats.vercel.app/api?username=T-Dynamos&show_icons=true&hide_border=true)
![Aakash's Language stats](https://github-readme-stats-eight-theta.vercel.app/api/top-langs/?username=T-Dynamos&layout=compact&langs_count=8&hide_border=true)
<br />
## Dontaions

If You really like my work you should consider donating to me to made me buy [Raspberry Pi 400](https://www.electronicscomp.com/raspberry-pi-400-personal-keyboard-computer-kit)

* UPI ID : anshdadwal@apl
